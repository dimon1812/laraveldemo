<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User,App\Role,App\Systemmodule;
use Illuminate\Http\Request;
use DB;
use Validator;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
         $this->middleware('auth');
    }

    public function index()
    {
        if(\Auth::user()->user_role==2){
            $user = User::select("users.*")
            ->rightJoin('user_role', 'users.id', '=', 'user_role.user_id')
            ->groupBy('users.id')
            ->get();
       
        }else{
            $user = User::where('id',\Auth::user()->id)->get();
        }
        return view('role.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $assignuser = Role::select("user_role.user_id")->where('user_role.status','1')->groupBy('user_id')->pluck('user_id');
        $user = User::whereNotIn('users.id',[$assignuser])->where('id','!=',\Auth::user()->id)->get();
        $modules = Systemmodule::where('status','1')->get();
        return view('role.create',compact('user','modules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId=$request->selectuser;
        $modules = Systemmodule::where('status','1')->get();
        foreach($modules as $modKey=>$modVal){
            $UserRole = new Role;
            $UserRole->user_id = $userId;
            $UserRole->can_view = (@$request->view[$modVal['id']]) ? '1' : '0';
            $UserRole->can_write = (@$request->add[$modVal['id']]) ? '1' : '0';
            $UserRole->can_edit = (@$request->edit[$modVal['id']]) ? '1' : '0';
            $UserRole->can_delete = (@$request->delete[$modVal['id']]) ? '1' : '0'; 
            $UserRole->module_id=$modVal['id'];
            $UserRole->save();
        }
        return redirect()->route('role.index')->with('success','User role has been created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $user = User::where('id',$id)->get();
        $modules = Role::with(['modulelist'])->select("user_role.*")
        ->leftJoin('system_module', 'user_role.module_id', '=', 'system_module.id')
        ->where('user_role.status','1')
        ->where('user_role.user_id',$id)->get();
        return view('role.show',compact('user','modules'));
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $user = User::where('id',$id)->get();
       
        $modules = Role::with(['modulelist'])->select("user_role.*","system_module.id as sysmid","system_module.id as smuid")
        ->leftJoin('system_module', 'user_role.module_id', '=', 'system_module.id')
        ->where('user_role.status','1')
        ->where('user_role.user_id',$id)->get();
          return view('role.edit',compact('user','modules'));

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $userId=$request['selectuser'];
        $modules = Systemmodule::where('status','1')->get();
        foreach($modules as $modKey=>$modVal){
            $UserRole = Role::find($request->rowid[$modVal['id']]);
            $UserRole->user_id = $userId;
            $UserRole->can_view = (@$request->view[$modVal['id']]) ? '1' : '0';
            $UserRole->can_write = (@$request->add[$modVal['id']]) ? '1' : '0';
            $UserRole->can_edit = (@$request->edit[$modVal['id']]) ? '1' : '0';
            $UserRole->can_delete = (@$request->delete[$modVal['id']]) ? '1' : '0'; 
            $UserRole->module_id=$modVal['id'];
            $UserRole->save();
        }
            
        return redirect()->route('role.index')->with('success','User role has been updated successfully');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function deleterole(Request $request,$id)
    {
        Role::where("user_id",$id)->delete();
        return redirect()->route('role.index')
        ->with('success','User role has been deleted successfully');
        
    }
    
}
