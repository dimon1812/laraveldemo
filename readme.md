
Please find the below steps for installation

1. Create database in phpmyamin name says 'testdemo'
2. Checkout the repo.
3. Go to that directory (For ex : C:/wamp64/www/testdemo)
4. Execute the following command for database table migration.
	php artisan migrate
5. Please add gmail credential to .env file located in root folder. (Email user name and password, Its compulsory for sending a email)
	MAIL_USERNAME=<Your gmail account email>
	MAIL_PASSWORD=<Your gmail account password>
6. Open the popular browser and open project link ( http://localhost/testdemo/public)

Note : In case migration script not working please import database manually with following login details
		Super Admin : superadmin@gmail.com / 123456
		Normal User : user@gmail.com / 123456
7. Execute following command		
	- PS D:\wamp64\www\testdemo> php artisan migrate
	- PS D:\wamp64\www\testdemo> php artisan db:seed --class=UsersTableSeeder

---------------------------------------------
---------------------------------------------

List of command excute during testing.

PS D:\wamp64\www> composer create-project --prefer-dist laravel/laravel testdemo "5.4.*"

PS D:\wamp64\www> cd testdemo

PS D:\wamp64\www\testdemo> php artisan make:auth

PS D:\wamp64\www\testdemo> php artisan migrate

PS D:\wamp64\www\testdemo> php artisan migrate:refresh

PS D:\wamp64\www\testdemo> php artisan db:seed --class=UsersTableSeeder

PS D:\wamp64\www\testdemo> php artisan config:cache

PS D:\wamp64\www\testdemo> php artisan make:model VerifyUser -m

PS D:\wamp64\www\testdemo> php artisan make:mail VerifyMail
 
