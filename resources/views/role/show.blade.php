
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading"> View Role</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('role.index') }}"> Back</a>
                            </div>
                        </div>
                    </div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="form-group">
								<strong>Name:</strong>
								{{ $user[0]['name'] }}  {{ $user[0]['lastname'] }}

							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="form-group">
								<strong>Email:</strong>
								{{ $user[0]['email'] }}
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th scope="col">Module Name</th>
											<th scope="col">Add</th>
											<th scope="col">Edit</th>
											<th scope="col">Delete</th>
											<th scope="col">View</th>
										</tr>
									</thead>
									<tbody>
									<?php 
									foreach($modules as $modulesKey=>$modulesVal){
										?>
										<tr>
											<td>	
												<?php echo $modulesVal['modulelist']['module_name'];?>
											</td>	
											<td>
												<input type="checkbox" disabled <?php if($modulesVal['can_write']==1) { echo "checked=checked"; } ?> name="add[<?php echo $modulesVal['id'];?>]">
											</td>	
											<td>
												<input type="checkbox" disabled <?php if($modulesVal['can_edit']==1) { echo "checked=checked"; } ?> name="edit[<?php echo $modulesVal['id'];?>]">
											</td>	
											<td>
												<input type="checkbox" disabled <?php if($modulesVal['can_delete']==1) { echo "checked=checked"; } ?> name="delete[<?php echo $modulesVal['id'];?>]">
											</td>	
											<td>
												<input type="checkbox"  disabled <?php if($modulesVal['can_view']==1) { echo "checked=checked"; } ?> name="view[<?php echo $modulesVal['id'];?>]">
											</td>
										<tr>
										<?php 
										}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>     
                </div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
    $("#loader").fadeOut("slow");
});
</script>
@endsection
