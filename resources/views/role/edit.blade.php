
@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading"> Edit Role</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                                    
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                                        
                                        
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('role.index') }}"> Back</a>
                            </div>
                        </div>
                    </div>
                   
                    <form action="{{ route('role.update',$user[0]['id']) }}" method="POST">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
									<strong>Selected User:</strong>
									<select name="selectuser" class="form-control" id="selectuser">
										<?php 
										foreach($user as $selectuserKey=>$selectuserVal){
											?>
											<option value="<?=$selectuserVal['id']?>"><?=$selectuserVal['name'].' '.$selectuserVal['last_name']?></option>
											<?php
										}
										?>
									</select>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Email:</strong>
                                    {{ $user[0]['email'] }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
											  <th scope="col">Module Name</th>
											  <th scope="col">Add</th>
											  <th scope="col">Edit</th>
											  <th scope="col">Delete</th>
											  <th scope="col">View</th>
											</tr>
										  </thead>
										  <tbody>
                                                <?php 
                                                foreach($modules as $modulesKey=>$modulesVal){
                                                ?>
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="rowid[<?=$modulesVal['smuid']?>]" value="<?=$modulesVal['id']?>">

                                                        <?php echo $modulesVal['modulelist']['module_name'];?>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox"  <?php if($modulesVal['can_write']==1) { echo "checked=checked"; } ?> name="add[<?php echo $modulesVal['sysmid'];?>]">
                                                    </td>
                                                    <td>
                                                        <input type="checkbox"  <?php if($modulesVal['can_edit']==1) { echo "checked=checked"; } ?> name="edit[<?php echo $modulesVal['sysmid'];?>]">
                                                    </td>
                                                    <td>
                                                        <input type="checkbox"  <?php if($modulesVal['can_delete']==1) { echo "checked=checked"; } ?> name="delete[<?php echo $modulesVal['sysmid'];?>]">
                                                    </td>
                                                    <td>
                                                        <input type="checkbox"   <?php if($modulesVal['can_view']==1) { echo "checked=checked"; } ?> name="view[<?php echo $modulesVal['sysmid'];?>]">
                                                    </td>
                                                <tr>
                                                 <?php 
                                                }
                                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $("#loader").fadeOut("slow");
});
</script>
@endsection