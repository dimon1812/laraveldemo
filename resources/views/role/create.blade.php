@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading"> Add Role</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                                    
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif                                        
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('role.index') }}"> Back</a>
                            </div>
                        </div>
					</div>
					
					<form action="{{ route('role.store') }}" id="signupForm" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
									<strong>Select User:</strong>
									<select name="selectuser" class="form-control" id="selectuser">
										<?php 
										foreach($user as $selectuserKey=>$selectuserVal){
											?>
											<option value="<?=$selectuserVal['id']?>"><?=$selectuserVal['name'].' '.$selectuserVal['last_name']?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
											  <th scope="col">Module Name</th>
											  <th scope="col">Add</th>
											  <th scope="col">Edit</th>
											  <th scope="col">Delete</th>
											  <th scope="col">View</th>
											</tr>
										  </thead>
										  <tbody>
									
											 
								
									<?php 
											foreach($modules as $modulesKey=>$modulesVal){
										?>
										<tr>
											<td>
												<?php echo $modulesVal['module_name'];?>
											</td>	
										<td>
											<input type="checkbox" name="add[<?php echo $modulesVal['id'];?>]">
										</td>
										<td>
											<input type="checkbox" name="edit[<?php echo $modulesVal['id'];?>]">
										</td>
										<td>
											<input type="checkbox" name="delete[<?php echo $modulesVal['id'];?>]">
										</td>
										<td>
											<input type="checkbox" name="view[<?php echo $modulesVal['id'];?>]">
										<td>
						<?php 
											}
										?>
										</tbody>
									</table>
									</div>
								</div>	
							</div>	
						
							<div class="col-xs-12 col-sm-12 col-md-12 text-center">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('page-js-script')
<script type="text/javascript">
    $(document).ready(function() {
	$("#signupForm").validate({
			rules: {
				firstname: "required",
				lastname: "required",
				name: {
					required: true,
					minlength: 2
				},
				password: {
					required: true,
					minlength: 5
				},
				password_confirmation: {
					required: true,
					minlength: 5,
					equalTo: "#password"
				},
				email: {
					required: true,
					email: true
				}
			},
			messages: {
				name: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 2 characters"
				},
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				password_confirmation: {
					required: "Please provide a confirm password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				},
				email: "Please enter a valid email address"
				
			}
		});
    });
</script>
<script>
    $(document).ready(function() {
    	 $("#loader").fadeOut("slow");
	});
</script>
@stop