@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading"> Role Management</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                                        
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                            
                            </div>
                            <?php if(\Auth::user()->user_role==2){?>
                            <div class="pull-right btn">
                                <a class="btn btn-success" href="{{ route('role.create') }}"> Assign Role</a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered"  width="100%" >
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach($user as $v){?>
                            <tr>
                                <td><?=$v['name']?></td>
                                <td><?=$v['lastname']?></td>
                                <td><?=$v['email']?></td>
                                <td>
                                    <form onsubmit="return confirm('Do you really want to delete the roles?');" action="{{ route('roledelete',$v->id) }}" method="POST">
                                        <a class="btn btn-info" href="{{ route('role.show',$v->id) }}">View Role</a>
                                        <?php if(\Auth::user()->user_role==2){?>
                                            <a class="btn btn-primary" href="{{ route('role.edit',$v->id) }}">Edit Role</a>    
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        
                                            <button type="submit" class="btn btn-danger">Delete Role</button>
                                        <?php } ?>
                                    </form>
                                </td>
                            </tr>
                            <?php }?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
     $("#loader").fadeOut("slow");
     });
     </script>
@endsection


