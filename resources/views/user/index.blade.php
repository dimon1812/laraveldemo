@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading"> User Listing</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                                        
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                            
                            </div>
                            <?php if(\Auth::user()->user_role==2){?>
                            <div class="pull-right btn">
                                <a class="btn btn-success" href="{{ route('user.create') }}"> Create New User</a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Phone Number</th>
                                    <th>Email</th>
                                    <th>Profile Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach($user as $v){?>
                                <tr>
                                    <td><?=$v['name']?></td>
                                    <td><?=$v['lastname']?></td>
                                    <td><?=$v['mobile']?></td>
                                    <td><?=$v['email']?></td>
                                    <td><?php
                                    if($v['profile_image']!=''){
                                    ?>
                                    <img width="100%" height="100%" style="width: 100px;height:100px;" src="{{URL::to('/')}}/uploads/<?php echo $v['profile_image'];?>"/>
                                    <?php
                                    }else{
                                        ?>
                                    <img width="100%" height="100%" style="width: 100px;height:100px;"  src="{{asset('images/noimage.png')}}"/>
                                    <?php
                                    
                                    }
                                    ?></td>
                                    <td>
                                        <form onsubmit="return confirm('Do you really want to delete the user?');" action="{{ route('user.destroy',$v->id) }}" method="POST">
                                            <a class="btn btn-info" href="{{ route('user.show',$v->id) }}">Show</a>
                                            <a class="btn btn-primary" href="{{ route('user.edit',$v->id) }}">Edit</a>    
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <?php if(\Auth::user()->user_role==2){?>
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            <?php } ?>
                                        </form>
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Phone Number</th>
                                    <th>Email</th>
                                    <th>Profile Image</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
     $("#loader").fadeOut("slow");
     });
     </script>
@endsection


