@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading"> Add User</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                                    
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif                                        
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('user.index') }}"> Back</a>
                            </div>
                        </div>
                    </div>
					<form action="{{ route('user.store') }}" id="createForm" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
									<strong>First Name:</strong>
									<input type="text" name="name" class="form-control" placeholder="First Name">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
									<strong>Last Name:</strong>
									<input type="text" name="lastname" class="form-control" placeholder="Last Name">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
									<strong>Email:</strong>
									<input type="text" name="email" class="form-control" placeholder="Email">
								</div>
							</div>
							   
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Mobile :</strong>
                                    <input type="text" name="mobile"  class="form-control" placeholder="Mobile">
                                </div>
                            </div>
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
									<strong>Password:</strong>
									<input id="password" type="password" class="form-control" name="password" required>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
									<strong>Confirm Password:</strong>
									<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 form-group{{ $errors->has('profile') ? ' has-error' : '' }}">
                                <label for="profile" class="col-md-4 control-label">Profile Image</label>
    
                                <div class="col-md-6">
                                    <div class="img-fluid"  style="height: 350px; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
                                        <img width="100%" height="100%" id="preview_image" src="{{asset('images/noimage.png')}}"/>
                                        <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                                    </div>
                                    <p>
                                        <a href="javascript:changeProfile()" style="text-decoration: none;">
                                            <i class="glyphicon glyphicon-edit"></i> Change
                                        </a>&nbsp;&nbsp;
                                        <a href="javascript:removeFile()" style="color: red;text-decoration: none;">
                                            <i class="glyphicon glyphicon-trash"></i>
                                            Remove
                                        </a>
                                    </p>
                                    <input type="file" id="file" name="file" style="display: none;" required/>
                                    <input type="hidden" name="file_name" id="file_name"/>
                                    @if ($errors->has('profile'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('profile') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="question"></div>
                            </div>
							<div class="col-xs-12 col-sm-12 col-md-12 text-center">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="Doiz" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
           <h4 class="modal-title" id="myModalLabel">Image Preview</h4>
         </div>
         <div class="modal-body">
           <img width="auto"  style="height:380px;max-width: 100%;"  id="preview_image_popup" src="{{asset('images/noimage.png')}}"/>
                                     
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
       </div>
     </div>
   </div>

@endsection
@section('page-js-script')
<script type="text/javascript">

    $(document).ready(function() {
    $("#loader").fadeOut("slow");
	$("#createForm").validate({
        ignore: [],
   		rules: {
            name: {
                required: true,
                minlength: 2
            },
            lastname: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 5
            },
            password_confirmation: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            mobile:{
                required: true,
                minlength: 10,
                maxlength:13
            },
            file:{
                required:true
            }
        },
        messages: {
            name: {
                required: "Please enter a first name",
                minlength: "Your first name must consist of at least 2 characters"
            },
            lastname: {
                required: "Please enter a last name",
                minlength: "Your last name must consist of at least 2 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            password_confirmation: {
                required: "Please provide a confirm password",
                minlength: "Your password must be at least 6 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: {
                required: "Please provide a email address",
                email: "Please enter a valid email address"
            },
            mobile:{
                required: "Please provide a mobile number"
            },
            file:{
                required: "Please upload profile image"
            }
        },
        errorPlacement: function(error, element) {

            if (element.attr("type") == "file") {
                error.insertAfter($(element).prev($('.question')));
            } else {
                error.insertAfter(element);
            }
        }
    });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    $("#preview_image").click(function(){
        $('#Doiz').modal('show');
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading').css('display', 'block');
        $.ajax({
            url: "{{url('ajax-image-upload')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '{{asset('images/noimage.png')}}');
                    alert(data.errors['file']);
                }
                else {
                    $('#file_name').val(data);
                    $('#preview_image').attr('src', '{{asset('uploads')}}/' + data);
                    $('#preview_image_popup').attr('src', '{{asset('uploads')}}/' + data);

                }
                $('#loading').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('images/noimage.png')}}');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '{{csrf_token()}}');
                $.ajax({
                    url: "{{url('ajax-remove-image')}}/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '{{asset('images/noimage.png')}}');
                        $('#file_name').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                       // alert(xhr.responseText);
                    }
                });
            }
    }
    
    function readURL(input) {
            if (input.files && input.files[0]) {
                upload(this);
             
                uploadImage();
            }
     }
</script>


@stop