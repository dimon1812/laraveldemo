@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading"> View User</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                                    
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif                                        
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('user.index') }}"> Back</a>
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('user.update',$user->id) }}" method="POST">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group col-xs-2">
                                    <strong>First Name:</strong>
                                </div> 
                                <div class="form-group col-xs-10 text-left">
                                    {{ $user->name }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group col-xs-2">
                                    <strong>Last Name:</strong>
                                </div> 
                                <div class="form-group col-xs-10 text-left">
                                    {{ $user->lastname }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group col-xs-2">
                                    <strong>Email:</strong>
                                </div> 
                                <div class="form-group col-xs-10 text-left">
                                    {{ $user->email }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group col-xs-2">
                                    <strong>Phone Numebr:</strong>
                                </div> 
                                <div class="form-group col-xs-10 text-left">
                                    {{ $user->mobile }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group col-xs-2">
                                    <strong>Profile Image:</strong>
                                </div> 
                                <div class="form-group col-xs-10 text-left">
                                    <?php
                                        if($user->profile_image!=''){
                                        ?>
                                        <img width="100%" height="100%" id="preview_image" style="width: 100px;height:100px;" src="{{URL::to('/')}}/uploads/<?php echo $user->profile_image;?>"/>
                                        <?php
                                        }else{
                                            ?>
                                        <img width="100%" height="100%" style="width: 100px;height:100px;"  src="{{asset('images/noimage.png')}}"/>
                                        <?php
                                        
                                        }
                                        ?>
                                </div>
                            </div>
                        
                        </div>
                    </form> 
                </div>
			</div>
		</div>
	</div>
</div>

<div id="Doiz" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
           <h4 class="modal-title" id="myModalLabel">Image Preview</h4>
         </div>
         <div class="modal-body">
           <img width="auto"  style="height:380px;max-width: 100%;"  id="preview_image_popup" src="{{URL::to('/')}}/uploads/<?php echo $user->profile_image; ?>"/>
                                     
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
       </div>
     </div>
   </div>

<script>
    $(document).ready(function() {
        $("#loader").fadeOut("slow");
        $("#preview_image").click(function(){
            $('#Doiz').modal('show');
        });
     });
     
     </script>
@endsection