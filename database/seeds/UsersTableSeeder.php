<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        
        //Single insert
        // DB::table('users')->insert([
        //     'name' => Str::random(10),
        //     'lastname' => Str::random(10),
        //     'mobile' => mt_rand(1111111111,9999999999), 
        //     'user_role' => '1',
        //     'email' => Str::random(10).'@gmail.com',
        //     'password' => bcrypt('password'),
        //     'profile_image'=>'defaultimage.png',
        //     'created_at'=>date('Y-m-d h:i:s')
        // ]);
        
        //Multi entry with model
        // factory(App\User::class, 5)->create()->each(function ($u) {
        //     //Inner logic if any
        // });

        //Multientry with faker
        $faker = Faker\Factory::create('en_IN');
        foreach (range(1,5) as $index) {
    
            User::create([
                'name' => $faker->firstNameMale,
                'lastname' => $faker->lastName,
                'mobile' =>  $faker->phoneNumber,
                'user_role' => '1',
                'email' => Str::random(10).'@gmail.com',
                'password' => bcrypt('password'),
                'profile_image'=>'defaultimage.png',
                'created_at'=>date('Y-m-d h:i:s')

            ]);
    
        }
    }
}
