<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_role', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->enum('can_view',['0', '1'])->default('0');
            $table->enum('can_write',['0', '1'])->default('0');
            $table->enum('can_edit',['0', '1'])->default('0');
            $table->enum('can_delete',['0', '1'])->default('0');   
            $table->string('module_id');
            $table->enum('status',['0', '1'])->default('1');   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_role');
    }
}
