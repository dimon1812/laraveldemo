<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_module', function (Blueprint $table) {
            $table->increments('id');
            $table->string('module_name');
            $table->enum('status',['0', '1'])->default('1');   
            $table->timestamp('created_at')->nullable();
        });
        //Default Module Entry
        DB::table('system_module')->insert(
            array(
                'module_name' => 'Product',
                'created_at' => date('Y-m-d h:i:s')
            )
        );
        DB::table('system_module')->insert(
            array(
                'module_name' => 'Category',
                'created_at' => date('Y-m-d h:i:s')
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_module');
    }
}
