<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::match(['get', 'post'], 'ajax-image-upload-reg', 'Auth\RegisterController@uploadImage');
Route::delete('ajax-remove-image-reg/{filename}', 'Auth\RegisterController@deleteImage');
Route::group(array('before' => 'auth'), function(){
    Route::get('/', 'HomeController@index');
    Route::resource('user','UserController');
    Route::delete('role/{id}/delete','RoleController@deleterole')->name('roledelete');

    Route::resource('role','RoleController');
    Route::match(['get', 'post'], 'ajax-image-upload', 'UserController@uploadImage');
    Route::delete('ajax-remove-image/{filename}', 'UserController@deleteImage');
});
